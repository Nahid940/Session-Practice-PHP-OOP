<?php
/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/19/2017
 * Time: 11:08 PM
 */
include_once '../../vendor/autoload.php';
\App\Session\Session::SessionInit();
\App\Session\Session::cehckSession();
$email=\App\Session\Session::get('email');
$admin=new \App\Admin\Admin();
$admin->userLogout();
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
<div class="container-fluid">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">

            <ul class="nav navbar-nav navbar-right">
                <li><a href="?action=logout"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
            </ul>
        </div>
    </nav>


    <div class="row">
        <div class="col-md-4">
            <h2>Hello <?php echo $email?></h2>
        </div>
    </div>
</div>

</body>
</html>

