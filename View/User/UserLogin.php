<?php
/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/19/2017
 * Time: 10:30 PM
 */
include_once '../../vendor/autoload.php';
$user=new \App\Test\User();


?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4">

            <?php
                if(isset($_POST['login'])){
                    $email=$_POST['email'];
                    $password=$_POST['password'];
                    $user->setEmail($email);
                    $user->setPassword($password);
                    if(!$user->userLogin()){
                        echo "<div class='alert alert-danger'>Something is not correct</div>";
                    }
                }
            ?>
            <h2>User login</h2>

            <form action="" method="post">
                <div class="form-group">
                    <label for="email">Enter email</label>
                    <input type="email" class="form-control" name="email">
                </div>

                <div class="form-group">
                    <label for="passsword">Enter password</label>
                    <input type="password" class="form-control" name="password">
                </div>
                <div class="form-group pull-right">
                    <button type="submit" name="login" class="btn btn-success">Login</button>
                </div>
            </form>

        </div>
    </div>
</div>

</body>
</html>

