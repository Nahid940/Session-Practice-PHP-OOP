<?php

/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/19/2017
 * Time: 10:43 PM
 */
namespace App\Session;
class Session
{
    public static function SessionInit(){
        session_start();
    }

    public static function set($key,$val){
        $_SESSION[$key]=$val;
    }

    public static function get($key){
        if(isset($_SESSION[$key])){
            return $_SESSION[$key];
        }else{
            return false;
        }
    }

    public static function DestroySession(){
        session_destroy();
        session_unset();
    }

    public static function cehckSession(){
        if(self::get('login')==false){
            header('location:AdminLogin.php');
        }
    }

    public static function cehckUserSession(){
        if(self::get('login')==false){
            header('location:UserLogin.php');
        }
    }



}