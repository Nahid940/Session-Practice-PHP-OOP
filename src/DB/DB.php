<?php

/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/19/2017
 * Time: 10:42 PM
 */
namespace App\DB;
class DB
{
    private static $pdo;
    public static function MyCon(){
        try{
            self::$pdo=new \PDO('mysql:host=localhost;dbname=mypractice',"root","");
        }catch(\PDOException $exp){
            return $exp->getMessage();
        }
        return self::$pdo;
    }

    public static function MyQuery($query){
        return self::MyCon()->prepare($query);
    }

}