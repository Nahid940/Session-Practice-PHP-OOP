<?php

/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/19/2017
 * Time: 10:41 PM
 */
namespace  App\Admin;
use App\DB\DB;
use App\Session\Session;

class Admin
{

private $email;
private $password;

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }


    public function AdminLogin(){

        if($this->email !='' && $this->password!='') {
            $sql = "select * from admin where email=:email";
            $stmt = \App\DB\DB::MyQuery($sql);
            $stmt->bindValue(':email', $this->email);
            $stmt->execute();
            $result = $stmt->fetch(\PDO::FETCH_OBJ);
            if ($stmt->rowCount() == 1 && $result) {
                Session::SessionInit();
                Session::set('login',true);
                Session::set('email', $result->email);
                header('location:Admin.php');
                return $result;
            } else {
                return false;
            }

        }
    }

    public function userLogout(){
        if(isset($_GET['action']) && $_GET['action']=='logout'){
            Session::DestroySession();
            header('location:AdminLogin.php');
        }
    }
}