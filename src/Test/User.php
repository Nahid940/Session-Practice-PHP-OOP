<?php

/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/20/2017
 * Time: 12:45 AM
 */
namespace App\Test;
use App\Session\Session;

class User
{
    private $email;
    private $password;

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function userLogin()
    {
        if ($this->email != '' && $this->password != '') {
            $sql = "select * from user where email=:email";
            $stmt = \App\DB\DB::MyQuery($sql);
            $stmt->bindValue(':email', $this->email);
            $stmt->execute();
            $result = $stmt->fetch(\PDO::FETCH_OBJ);
            if ($stmt->rowCount() == 1) {
                \App\Session\Session::SessionInit();
                \App\Session\Session::set('login',true);
                \App\Session\Session::set('email', $result->email);
                header('location:User.php');
                return $result;
            } else {
                return false;
            }
        }

    }

    public function UserLogOut(){
        if(isset($_GET['action']) && $_GET['action']=='logout'){
            Session::DestroySession();
            header('location:UserLogin.php');
        }
    }
}